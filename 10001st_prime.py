#Credit to Corey Yuhas whose solution helped me made mine way more efficient and fast
def finding_prime_number_rank(rank):
    i = 5
    current_rank = 2
    primes = [2,3]
    while current_rank != rank:
        is_prime = True
        stop = i ** 0.5 
        for prime in primes:
            if i % prime == 0:
                is_prime = False
                break
            if prime > stop:
                break
        if is_prime:
            primes.append(i)
            current_rank += 1
        i += 2
    return primes[-1]

print(finding_prime_number_rank(10001))